# Pose_Detection_counter_for_Gym_Exercises

This project was an experiment to count the number of repetitions during a gym workout. I implemented it for three different kind of exercises, such as push-ups, pull-ups and squats. I have used multiple dependencies for person detection, pose and body joints detection.
# Pullups counter
![Pullups](/pullups-out-zibran.gif)
# Pushups Counter
![Pushups](/pushups-3reps.gif)
# Squats 
![Squats](/squats-out.gif)
# End
